import Vue from "vue";
import Router from "vue-router";

import AuditTrail from "../components/AuditTrail";
import Files from "../components/Files";
import FilesManage from "../components/FilesManage";
import Lookup from "../components/Lookup";
import News from "../components/News";
import Notes from "../components/Notes";
import Semester from "../components/Semester";
import Statistics from "../components/Statistics";
import Supporters from "../components/Supporters";
import Users from "../components/Users";
import Events from "../components/Events";
import Eventslist from "../components/Eventslist";
import Edit from "../components/Edit";
import Mail from "../components/Users/Mail";
import Votes from "../components/Votes";
import VotesManage from "../components/VotesManage";
import Login from "../components/Login";
import ChangePW from "../components/ChangePW";

import { auth } from "./feathers";


Vue.use(Router);
// create and export router
const router = new Router({
  routes: [
    { path: "/", redirect: "/news" },
    { path: "/news", component: News },
    { path: "/login", name: "login", component: Login },
    { path: "/changepw", name: "changepw", component: ChangePW },
    { path: "/files", component: Files },
    { path: "/votes", component: Votes },
    {
      path: "/mail", name: "mail", component: Mail, props: true
    },
    { path: "/users", name: "users", component: Users },
    { path: "/edit/:id", name: "edit", component: Edit },
    { path: "/new", name: "new", component: Edit },
    { path: "/me", name: "me", component: Edit },
    { path: "/events", name: "events", component: Events },
    { path: "/Eventslist", name: "Eventslist", component: Eventslist },
    {
      path: "/edit/:id", name: "edit", component: Edit
    },
    {
      path: "/new", name: "new", component: Edit
    },
    {
      path: "/me", name: "me", component: Edit
    },
    { path: "/supporters", component: Supporters },
    { path: "/semester", component: Semester },
    { path: "/notes", component: Notes },
    { path: "/lookup", component: Lookup },
    { path: "/audit", component: AuditTrail },
    { path: "/files-manage", component: FilesManage },
    { path: "/votes-manage", component: VotesManage },
    { path: "/statistics", component: Statistics },
  ]
});
// ensure authentication is setup
router.beforeEach((to, from, next) => {
  if (!auth.initialized) {
    return auth.setup()
      .then(() => next());
  }
  return next();
});
// special handling for login / logout path
// eslint-disable-next-line consistent-return
router.beforeEach((to, from, next) => {
  if (auth.loggedin && to.name === "login") {
    return next("/");
  } else if (!auth.loggedin && to.name !== "login") {
    return next("/login");
  } else if (auth.loggedin && to.path === "/logout") {
    auth.logout();
  }
  return next();
});
export default router;
