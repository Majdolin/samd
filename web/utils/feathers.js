import Feathers from "@feathersjs/feathers";
import rest from "@feathersjs/rest-client";
import Injector from "vue-inject";
import fauth from "@feathersjs/authentication-client";

// create feathers client instance
const feathers = Feathers()
  .configure(rest()
    .fetch(window.fetch.bind(window)))
  .configure(fauth({
    storage: window.localStorage
  }));
export default feathers;

async function processToken(accessToken) {
  const uid = JSON.parse(window.atob(accessToken.split(".")[1])).userId;
  const user = await feathers.service("users").get(uid);
  auth.user = user; // eslint-disable-line no-use-before-define
  auth.loggedin = true; // eslint-disable-line no-use-before-define
}

export const auth = {
  user: null,
  loggedin: false,
  initialized: false,
  /**
   * setup authentication
   * @return {Promise<>} promise resolves when login succeeds
   */
  async setup() {
    try {
      // try to authenticate with stored credentials
      const { accessToken } = await feathers.authenticate({});
      await processToken(accessToken);
    } catch (ignore) {
      auth.initialized = true;
      return false;
    }
    auth.initialized = true;
    return true;
  },
  /**
   * check if user has a certain role
   * @param {string|string[]} role role to check
   * @return {Boolean}
   */
  hasRole(role) {
    if (!auth.loggedin || !auth.user || !auth.user.roles) {
      return false;
    }
    if (auth.user.roles.indexOf("Admin") >= 0) {
      // admin is allowed to do anything
      return true;
    }
    if (Array.isArray(role)) {
      return role.some(r => auth.hasRole(r));
    }
    return auth.user.roles.indexOf(role) >= 0;
  },
  /**
   * login
   * @param mail
   * @param password
   * @return {Promise<void>}
   */
  async login(mail, password) {
    if (auth.loggedin) {
      return;
    }
    if (!mail || !password) {
      throw new Error("invalid data provided");
    }
    const { accessToken } = await feathers.authenticate({
      strategy: "local",
      mail: mail.toLowerCase(),
      password
    });
    await processToken(accessToken);
  },
  /**
   * logout
   */
  logout() {
    feathers.logout();
    window.location.reload();
  }
};

// inject feathers service getter as 'api'
Injector.factory("api", () => service => feathers.service(service));

// inject auth
Injector.factory("auth", () => auth);

