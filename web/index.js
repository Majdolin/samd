import Vue from "vue";
import Vuetify from "vuetify";
import Injector from "vue-inject";
// import vuetify styles
import "vuetify/src/stylus/main.styl";
// import webfonts
import "./styles/fonts.css";
import router from "./utils/router";
import App from "./components/App";
import "./utils/feathers";

import "vue-googlemaps/dist/vue-googlemaps.css"
import VueGoogleMaps from 'vue-googlemaps'
Vue.use(VueGoogleMaps, {
  load: {
    // Google API key
    apiKey: "AIzaSyCkAfS938AfhwbnN8SamscP6b3k-ONFszg",
    // Enable more Google Maps libraries here
    libraries: ["places"],
    // Use new renderer
    useBetaRenderer: false,
  },
});

// add support for dependency injection
Vue.use(Injector);
// add vuetify functionality
Vue.use(Vuetify);
// disable information about production use
Vue.config.productionTip = false;


// bootstrap app
// eslint-disable-next-line no-new
new Vue({
  el: "#app",
  components: { App },
  template: "<App/>",
  router
});

