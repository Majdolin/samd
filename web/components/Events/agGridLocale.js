export default {
  // https://ag-grid.com/javascript-grid-internationalisation

  // for filter panel
  // page: "daPage",
  // more: "daMore",
  // to: "daTo",
  // of: "daOf",
  // next: "daNexten",
  // last: "daLasten",
  // first: "daFirsten",
  // previous: "daPreviousen",
  // loadingOoo: "daLoading...",

  // for set filter
  // selectAll: "daSelect Allen",
  // searchOoo: "daSearch...",
  // blanks: "daBlanc",

  // for number filter and text filter
  filterOoo: "Filter",
  // applyFilter: "daApplyFilter...",

  // for number filter
  equals: "entspricht",
  notEqual: "entspricht nicht",
  lessThan: "kleiner als",
  lessThanOrEqual: "kleiner gleich",
  greaterThan: "größer als",
  greaterThanOrEqual: "größer gleich",
  inRange: "zwischen",


  // for text filter
  contains: "enthält",
  notContains: "enthält nicht",
  startsWith: "beginnt mit",
  endsWith: "endet mit",

  // the header of the default group column
  // group: "laGroup",

  // tool panel
  // columns: "laColumns",
  // rowGroupColumns: "laPivot Cols",
  // rowGroupColumnsEmptyMessage: "la drag cols to group",
  // valueColumns: "laValue Cols",
  // pivotMode: "laPivot-Mode",
  // groups: "laGroups",
  // values: "laValues",
  // pivots: "laPivots",
  // valueColumnsEmptyMessage: "la drag cols to aggregate",
  // pivotColumnsEmptyMessage: "la drag here to pivot",

  // other
  noRowsToShow: "Keine Datensätze",
  loadingOoo: "Lade...",

  /*

  // enterprise menu
  pinColumn: "laPin Column",
  valueAggregation: "laValue Agg",
  autosizeThiscolumn: "laAutosize Diz",
  autosizeAllColumns: "laAutsoie em All",
  groupBy: "laGroup by",
  ungroupBy: "laUnGroup by",
  resetColumns: "laReset Those Cols",
  expandAll: "laOpen-em-up",
  collapseAll: "laClose-em-up",
  toolPanel: "laTool Panelo",
  export: "laExporto",
  csvExport: "la CSV Exportp",
  excelExport: "la Excel Exporto",

  // enterprise menu pinning
  pinLeft: "laPin <<",
  pinRight: "laPin >>",
  noPin: "laDontPin <>",

  // enterprise menu aggregation and status panel
  sum: "laSum",
  min: "laMin",
  max: "laMax",
  first: "laFirst",
  last: "laLast",
  none: "laNone",
  count: "laCount",
  average: "laAverage",

  // standard menu
  copy: "laCopy",
  copyWithHeaders: "laCopy Wit hHeaders",
  ctrlC: "ctrl n C",
  paste: "laPaste",
  ctrlV: "ctrl n C"

  */
};
