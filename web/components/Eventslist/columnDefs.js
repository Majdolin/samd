import FilterSet from "./FilterSet";

export default [
  // https://ag-grid.com/javascript-grid-column-properties/
  {
    checkboxSelection: true,
    headerCheckboxSelection: true,
    sort: false,
    editable: false,
    suppressFilter: true,
    suppressResize: true,
    suppressSizeToFit: true,
    width: 36,
    maxWidth: 36,
    cellStyle: {
      // disable ellipsis
      textOverflow: "clip"
    },
    pinned: "left",
    lockPosition: true
  },
  {
    headerName: "Id",
    filter: "number",
    field: "id",
    cellStyle: { textAlign: "right" }
  },
  { headerName: "Beschreibung", field: "text" },


];
