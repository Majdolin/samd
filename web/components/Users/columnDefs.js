import FilterSet from "../Events/FilterSet";

export default [
  // https://ag-grid.com/javascript-grid-column-properties/
  {
    checkboxSelection: true,
    headerCheckboxSelection: true,
    sort: false,
    editable: false,
    suppressFilter: true,
    suppressResize: true,
    suppressSizeToFit: true,
    width: 36,
    maxWidth: 36,
    cellStyle: {
      // disable ellipsis
      textOverflow: "clip"
    },
    pinned: "left",
    lockPosition: true
  },
  {
    headerName: "Mitglied",
    filter: "number",
    field: "mid",
    cellStyle: { textAlign: "right" }
  },
  { headerName: "Vorname", field: "firstName" },
  { headerName: "Nachname", field: "lastName" },
  {
    headerName: "Anrede",
    field: "gender",
    // filterFramework: FilterSet,
    filterParams: {
      values: ["Frau", "Herr", ""]
    }
  },
  { headerName: "Addresszeile 1", field: "address1" },
  { headerName: "Addresszeile 2", field: "address2" },
  { headerName: "PLZ", field: "zip" },
  {
    headerName: "Ort",
    field: "city",
    // filterFramework: FilterSet,
    filterParams: {
      values: []
    }
  },
  { headerName: "Telefon", field: "phone" },
  { headerName: "E-Mail", field: "mail" },
  {
    headerName: "Geburtsdatum",
    field: "dob",
    valueFormatter({ value }) {
      if (!value) {
        return "";
      }
      const [year, month, day] = value.split("-");
      return `${day}.${month}.${year}`;
    },
    filter: "date",
    filterParams: {
      comparator(dateAtMidnight, cellValue) {
      // compare filter dateAtMidnight to cell value
      // return -1 if cell value is smaller, 1 if its larger, 0 if it's the same
        const [year, month, day] = cellValue.split("-");
        const date = new Date(year, month - 1, day);
        if (date < dateAtMidnight) {
          return -1;
        } else if (date > dateAtMidnight) {
          return 1;
        }
        return 0;

      // return new Date(cellValue).getTime() - dateAtMidnight.getTime();
      }
    }
  },
  { headerName: "Studienfach / Beruf", field: "studyField" },
  {
    headerName: "Stimme",
    field: "voice",
    // filterFramework: FilterSet,
    filterParams: {
      values: ["Sopran", "Alt", "Tenor", "Bass", "Chorleiter"]
    }
  },
  {
    headerName: "Stimmgruppe",
    field: "voiceGroup",
    // filterFramework: FilterSet,
    filterParams: {
      values: ["I", "II", ""]
    }
  },
  {
    headerName: "Notenpfand bezahlt",
    field: "pledge",
    // filterFramework: FilterSet,
    filterParams: {
      values: ["Bezahlt", "Nicht bezahlt", ""]
    }
  },
  {
    headerName: "Rollen",
    field: "roles",
    // filterFramework: FilterSet,
    filterParams: {
      values: ["Mitglied", "Vorstand", "Kassenwart", "Notenwart", "Stimmführer", "Admin"]
    }
  },
  {
    headerName: "Semester",
    field: "activeSemesters",
    tooltipField: "activeSemesters"
  },
  {
    headerName: "Letzer Login",
    field: "lastLogin",
    valueFormatter({ value }) {
      if (!value) {
        return "";
      }
      const [year, month, day] = value.split("-");
      return `${day}.${month}.${year}`;
    },
    filter: "date",
    filterParams: {
      comparator(dateAtMidnight, cellValue) {
        // compare filter dateAtMidnight to cell value
        // return -1 if cell value is smaller, 1 if its larger, 0 if it's the same
        const [year, month, day] = cellValue.split("-");
        const date = new Date(year, month - 1, day);
        if (date < dateAtMidnight) {
          return -1;
        } else if (date > dateAtMidnight) {
          return 1;
        }
        return 0;
      }
    }
  },
  {
    headerName: "Aktiv",
    field: "active",
    // editable: true,
    valueGetter: ({ data: { active } }) => (active ? "Ja" : "Nein"),
    filterFramework: FilterSet,
    filterParams: {
      values: ["Ja", "Nein"]
    },
    comparator(formattedA, formattedB, nodeA, nodeB) {
      const valueA = nodeA.data.active;
      const valueB = nodeB.data.active;
      if (valueA === valueB) {
        return 0;
      }
      return valueA ? 1 : -1;
    }
  },

];
