// http://eslint.org/docs/user-guide/configuring
const path = require("path");

module.exports = {
  env: {
    browser: true
  },
  // plugins: ["vue"],
  // check if imports actually resolve
  // FIXME

  settings: {
    "import/resolver": {
      webpack: {
        config: path.resolve(__dirname, "..", "webpack.config.js") // TODO
      }
    },
    "import/extensions": [".js", ".vue"]
  }
}
