// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: "babel-eslint",
    sourceType: "module",
    ecmaVersion: 2017
  },
  env: {
    browser: true,
    es6: true
  },
  extends: [
    "plugin:vue/recommended",
    "airbnb-base"
  ],
  // required to lint *.vue files
  plugins: [
    "import",
    "vue",
    "html",
  ],
  settings: {
    "import/ignore": [/\.vue$/]
  },
  // add your custom rules here
  "rules": {
    "no-plusplus": 0,
    "arrow-parens": 0,
    "eol-last": 0,
    "comma-dangle": 0,
    "quotes": ["error", "double"],
    // don"t require .vue extension when importing
    "import/extensions": ["error", "always", {
      "js": "never",
      "vue": "never"
    }],
    // allow optionalDependencies
    "import/no-extraneous-dependencies": ["error", {
      // FIXME
      // "optionalDependencies": ["test/unit/index.js"]
    }],
    // allow debugger during development
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
    "function-paren-newline": ["error", "never"]
  }
}
