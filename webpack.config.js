/* eslint-env node */
const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");


module.exports = {
  devtool: "source-map", // TODO
  context: path.resolve(__dirname, "web"),
  entry: {
    app: [
      "whatwg-fetch", // fetch polyfill
      "./index.js"
    ]
  },
  output: {
    publicPath: "/",
    path: path.resolve(__dirname, "build"),
    filename: "[name].js"
  },
  resolve: {
    extensions: [".js", ".vue", ".json"],
    alias: {
      vue$: "vue/dist/vue.esm.js",
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || "development")
      }
    }),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "index.html",
      inject: "body"
    }),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, "public")
      }
    ]),
    new webpack.ProvidePlugin({
      Promise: "bluebird"
    })
  ],
  module: {
    rules: [
      /*
       {
       test: /\.(js|vue)$/,
       loader: "eslint-loader",
       enforce: "pre",
       include: [resolve("src"), resolve("test")],
       options: {
       formatter: require("eslint-friendly-formatter")
       }
       },
       */
      {
        test: /\.vue$/,
        loader: "vue-loader",
        // FIXME: vueLoaderConfig
        options: {
          loaders: {
            scss: "vue-style-loader!css-loader!resolve-url-loader!sass-loader"
          }
        }
      },
      {
        test: /\.js$/,
        loader: "babel-loader",
        exclude: /node_modules/
      },
      {
        test: /\.styl$/,
        loader: ["style-loader", "css-loader?sourceMap", "stylus-loader?sourceMap"]
      },
      {
        test: /\.css$/,
        loader: ["style-loader", "css-loader?sourceMap"]
      },
      {
        test: /\.scss$/,
        loader: ["style-loader", "css-loader?sourceMap", "resolve-url-loader?sourceMap", "sass-loader?sourceMap"]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: "url-loader",
        options: {
          limit: 10000,
          name: "img/[name].[hash:7].[ext]"
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: "url-loader",
        options: {
          limit: 10000,
          name: "fonts/[name].[hash:7].[ext]"
        }
      }
    ]
  }
};

if (process.env.NODE_ENV === "development") {
  // eslint-disable-next-line global-require
  const FriendlyErrorsPlugin = require("friendly-errors-webpack-plugin");
  module.exports.plugins.push(new FriendlyErrorsPlugin());
} else {
  module.exports.plugins.push(new UglifyJsPlugin({
    sourceMap: true
  }));
}

if (process.env.DOSTATS) {
  // eslint-disable-next-line global-require
  const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
  module.exports.plugins.push(new BundleAnalyzerPlugin({
    analyzerMode: "static",
    reportFilename: "stats.html",
  }));
}
