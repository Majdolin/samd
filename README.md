## TODO

- vue-kindergarten
- authentication
- eslint-loader for prod
- postcss: prefix
- ie block
- browserslist
- tests
- favicons (larger)
- editorconfig export
- coverage: istanbul
- express: static loader
- configurable url loader
