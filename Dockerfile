# This Dockerfile makes it possible to put the whole project into a docker container which runs the application automatically,
# quickly and reliably from one computing environment to another without having the need to install any dependencies on
# your own
# See https://www.docker.com/ for more details

FROM node:8.14

# Make the 'app' folder the current working directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# If you are building your code for production
# RUN npm ci --only=production
# Npm used to install yarn as a packet manager
# Then install project dependencies through yarn
RUN npm install -g yarn
RUN yarn install


# build app for production
ENV NODE_ENV production
EXPOSE 8000
CMD [ "yarn", "start" ]


# How to use this file:
# Download Docker Desktop from docker.com and install the program
# In the console, terminal or shell:
# 1. Check if docker installed correctly by typing in: "docker --version" and make sure it is is open and running (e.g system tray icon)
# 2. Navigate to this project folder (in console/terminal/shell)
# 3. Type: docker build . -t <your image name here all lowercase without "<" or ">"> , which creates a docker image out of this Dockerfile
# WARNING: Node takes up ~900MB of space and the full container ~1.3gb
# 4. You can check if the image has been created successfully by typing in: "docker images" and finding the name specified in 3.
# 5. Afterwards type in:
#    docker run -p <PORT NUMBER you want to use on your machine>:8000 -d <IMAGE NAME from 3. here>
#    the port number you choose will be the entry point through localhost in your browser for example:
#    docker run -p 5000:8000 -d myimagename -> accessible at localhost:5000
# 6. You can check if the container has been created successfully by typing in: "docker ps" (lists all available and running containers)
# 7. (Optional) Log Commands:
#    With the container id you can check logs by typing in: "docker logs <CONTAINER ID(see 6.)>"
#    You should check logs after a few seconds to give time to fully build the application
#    The project is fully build when it says: "webpack: Compiled successfully." in the logs
# 8. (Optional) Stopping and deleting Containers and Images:
#    Stop the container by typing in: "docker stop <CONTAINER ID (see 6.)>"
#    Remove images with: docker rmi <IMAGE ID (see 4.)>, or forced: docker rmi -f <IMAGE ID>
#    Remove containers with: docker rm <CONTAINER ID>
# Reminder: Navigate with arrow keys up/down to get back to previous commands faster instead of typing them in
