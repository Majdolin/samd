// setup config
const Promise = require("bluebird");
const nconf = require("./utils/config");
const log = require("./utils/log");

// add log globally
global.log = log;

// list of units to load
const unitsLoaded = [];

/**
 * run a unit
 * @param {string} unitName name of unit
 * @returns {Promise} promise which resolves when unit loads successfully
 */
function runUnit(unitName) {
  if (typeof unitName !== "string" || unitName.length === 0) {
    // log.error("invalid unit name provided");
    return Promise.reject();
  }
  if (unitsLoaded.indexOf(unitName) >= 0) {
    log.debug("skipping unit '%s': already loaded", unitName);
    return Promise.resolve();
  }
  return Promise.try(() => {
    log.debug(`loading unit: ${unitName}`);
    unitsLoaded.push(unitName);
    // eslint-disable-next-line global-require, import/no-dynamic-require
    return require(`./units/${unitName}`);
  })
    .catch((err) => {
      log.error("could not load unit '%s'", unitName);
      log.debug(err);
      return Promise.reject();
    })
    .then((unit) => {
      log.debug("unit '%s' loaded, resolving dependencies", unitName);
      return Promise.map((unit.dependencies || []),
        dep => Promise.resolve(runUnit(dep)),
        { concurrency: 1 })
        .return(unit);
    })
    .then((unit) => {
      log.debug("depencendies resolved, starting unit '%s'", unitName);
      return Promise.try(() => Promise.resolve(unit.run()))
        .catch((err) => {
          log.error(err);
          return Promise.reject(new Error(`failed to start unit '${unitName}'`));
        });
    });
}
const mode = nconf.get("mode");
// log.debug("starting in mode: %s", mode);
runUnit(mode)
  .then(() => {
    log.debug("startup complete");
  }, (err) => {
    log.error("startup failed: %s", err.message);
    process.exit(1);
  });
