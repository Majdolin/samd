const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() { // create table to save datas.
  if (!await db.schema.hasTable("notes")) {
    await db.schema.createTable("notes", table => {
      table.increments("id");
      table.string("vorname");
      table.string("name");
      table.string("zurueckgegeben");
      table.date("bezahlt");
      table.string("bezahlart");
      table.string("noten");
      table.date("austrittsjahr");
      table.string("bemerkungen");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/notes", fknex({
    Model: db,
    name: "notes"
  }));
  app.service("notes").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.requireRole("Admin"),
        securityHooks.requireRole("Notenwart")
      ],
      create: [
        securityHooks.requireRole("Admin"),
        securityHooks.requireRole("Notenwart")
      ],
      patch: [
        securityHooks.requireRole("Admin"),
        securityHooks.requireRole("Notenwart")
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};
