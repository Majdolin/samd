const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
    if (!await db.schema.hasTable("voteschoices")) {
        await db.schema.createTable("voteschoices", table => {
            table.increments("id");
            table.int("idvotes");
            table.foreign("idvotes").references("votes.id");
            table.string("choice");
            table.int("count");// patch by mitglied
        });
    }
}

module.exports = async app => {
    await createTable();
    app.use("/voteschoices", fknex({
        Model: db,
        name: "voteschoices"
    }));
    app.service("voteschoices").hooks({
        before: {
            all: [
                securityHooks.requireAuth()
            ],
            remove: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            patch: [
                securityHooks.requireRole("Mitglied")
            ],
        }
    });
};

