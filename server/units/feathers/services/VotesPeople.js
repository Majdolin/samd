const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  if (!await db.schema.hasTable("votespeople")) {
    await db.schema.createTable("votespeople", table => {
      table.increments("id");
      table.int("voteid");
      table.foreign("voteid").references("votesgroup.id");
      table.int("userid");
      table.foreign("userid").references("user.id");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/votespeople", fknex({
    Model: db,
    name: "votespeople"
  }));
  app.service("votespeople").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.requireRole("Vorstand") // ("Admin")
      ],
      create: [
        securityHooks.requireRole("Vorstand") // ("Admin")
      ],
      patch: [
        securityHooks.requireRole("Mitglied")
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};

