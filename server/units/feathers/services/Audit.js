const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  // await db.schema.dropTableIfExists("audit");
  if (!await db.schema.hasTable("audit")) {
    await db.schema.createTable("audit", table => {
      table.increments("id");
      // timestamp of entry
      table.integer("timestamp").notNullable();
      // service name
      table.string("service").notNullable();
      // member that triggered action
      table.integer("mid").notNullable();
      // target id (optional)
      table.string("target");
      // method used
      table.string("method").notNullable();
      // payload of edit
      table.json("payload").notNullable();
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/audit", fknex({
    Model: db,
    name: "audit"
  }));
  app.service("audit").hooks({
    before: {
      all: [
        securityHooks.requireAuth(),
        securityHooks.requireRole("Admin")
      ],
      create: [
        securityHooks.disable()
      ],
      patch: [
        securityHooks.disable()
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
  // setup global audit hook
  app.hooks({
    after: {
      all: [
        securityHooks.audit()
      ]
    }
  });
};
