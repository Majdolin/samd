const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
    if (!await db.schema.hasTable("peoplegroup")) {
        await db.schema.createTable("peoplegroup", table => {
            table.increments("id");
            table.int("groupid");
            table.foreign("groupid").references("groups.id");
            table.int("userid");
            table.foreign("userid").references("user.id");
        });
    }
}

module.exports = async app => {
    await createTable();
    app.use("/peoplegroup", fknex({
        Model: db,
        name: "peoplegroup"
    }));
    app.service("peoplegroup").hooks({
        before: {
            all: [
                securityHooks.requireAuth()
            ],
            remove: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            create: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            patch: [
                securityHooks.requireRole("Mitglied")
            ],
            update: [
                securityHooks.disable()
            ]
        }
    });
};

