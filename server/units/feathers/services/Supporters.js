const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() { // create table to save datas.
  if (!await db.schema.hasTable("supporters")) {
    await db.schema.createTable("supporters", table => {
      table.increments("id");
      table.string("vorname");
      table.string("name");
      table.date("geburtsdatum");
      table.date("bezahlt");
      table.date("bezahlart");
      table.string("adresse");
      table.date("austrittsjahr");
      table.string("bemerkungen");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/supporters", fknex({
    Model: db,
    name: "supporters"
  }));
  app.service("supporters").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.requireRole("Admin"),
      ],
      create: [
        securityHooks.requireRole("Admin"),
      ],
      patch: [
        securityHooks.requireRole("Admin"),
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};
