const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
    if (!await db.schema.hasTable("peoplechoices")) {
        await db.schema.createTable("peoplechoices", table => {
            table.increments("id");
            table.int("idusers");
            table.foreign("idusers").references("users.id");
            table.int("idvote");
            table.foreign("idvote").references("votes.id");
            table.int("idchoice");
            table.foreign("idchoice").references("voteschoices.id");
        });
    }
}

module.exports = async app => {
    await createTable();
    app.use("/peoplechoices", fknex({
        Model: db,
        name: "peoplechoices"
    }));
    app.service("peoplechoices").hooks({
        before: {
            all: [
                securityHooks.requireAuth()
            ],
            remove: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            create: [],
            patch: [
                securityHooks.requireRole("Mitglied")
            ],
            update: [
                securityHooks.disable()
            ]
        }
    });
};

