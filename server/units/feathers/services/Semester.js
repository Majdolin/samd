const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  if (!await db.schema.hasTable("semester")) {
    await db.schema.createTable("semester", table => {
      table.increments("id");
      table.string("type");
      table.integer("year");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/semester", fknex({
    Model: db,
    name: "semester"
  }));
  const semester = await app.service("semester").find();
  if (!semester.length) {
    await app.service("semester").create({
      id: 1,
      type: "WS",
      year: new Date().getFullYear()
    });
  }
  app.service("semester").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.disable()
      ],
      create: [
        securityHooks.disable()
      ],
      patch: [
        securityHooks.requireRole("Admin")
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};
