const { sendTemplate } = require("../../../../utils/mails");
const generatePassword = require("password-generator");
// const securityHooks = require("../../../../utils/securityHooks");

class Forgot {
  constructor(app) {
    this.app = app;
  }
  async create(data) {
    if (!data || typeof data.mail !== "string") {
      throw new Error("invalid request");
    }
    const users = this.app.service("users");
    const user = await users.find({
      query: {
        mail: data.mail,
        $limit: 1,
        $select: ["mail", "firstName"]
      }
    });
    if (user.length !== 1) {
      throw new Error("not found");
    }
    const pwd = generatePassword();
    await users.patch(user[0].id, {
      password: pwd
    });
    await sendTemplate(user[0].mail, "newpassword", {
      firstName: user[0].firstName,
      password: pwd
    });
    return { success: true };
  }
}


module.exports = app => {
  app.use("forgot", new Forgot(app));
  app.service("forgot").hooks({
    before: {

    }
  });
};
