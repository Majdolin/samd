const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
    if (!await db.schema.hasTable("votesgroup")) {
        await db.schema.createTable("votesgroup", table => {
            table.increments("id");
            table.bool("active");
            // table.string("expiryDate", 10).notNullable();
            table.time("expiryDate", {precision: 6}).notNullable();
            table.string("name").notNullable();
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            var today = yyyy + '-' + mm + '-' + dd;
            table.timestamp("created_at").defaultTo(today);
            table.string("year").notNullable().defaultTo(yyyy);


        });
        return true;
    }
    return false;
}

module.exports = async app => {
    await createTable();
    app.use("/votesgroup", fknex({
        Model: db,
        name: "votesgroup"
    }));
    app.service("votesgroup").hooks({
        before: {
            all: [
                securityHooks.requireAuth()
            ],
            remove: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            create: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            patch: [
                securityHooks.requireRole("Vorstand")
            ],
        }
    });
};
