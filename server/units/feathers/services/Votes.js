const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  if (!await db.schema.hasTable("votes")) {
    await db.schema.createTable("votes", table => {
      table.increments("id");
      table.string("question").notNullable();
      table.string("type", 30).notNullable();
      table.int("voteid");
      table.foreign("voteid").references("votesgroup.id");
    });
    return true;
  }
  return false;
}

module.exports = async app => {
  await createTable();
  app.use("/votes", fknex({
    Model: db,
    name: "votes"
  }));
  app.service("votes").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.requireRole("Vorstand") // ("Admin")
      ],
      create: [
        securityHooks.requireRole("Vorstand") // ("Admin")
      ],
      patch: [
        securityHooks.requireRole("Vorstand")
      ],
    }
  });
};
