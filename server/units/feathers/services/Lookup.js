const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  if (!await db.schema.hasTable("lookup")) {
    await db.schema.createTable("lookup", table => {
      table.increments("id");
      table.string("name");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/lookup", fknex({
    Model: db,
    name: "lookup"
  }));
  app.service("lookup").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.requireRole("Admin")
      ],
      create: [
        securityHooks.requireRole("Admin")
      ],
      patch: [
        securityHooks.requireRole("Admin")
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};
