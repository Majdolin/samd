const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  if (!await db.schema.hasTable("joinevents")) {
    await db.schema.createTable("joinevents", table => {
      table.increments("id");
      table.text("uid").notNullable().defaultTo("");
      table.text("eid").defaultTo("");
      table.text("date").defaultTo("");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/joinevents", fknex({
    Model: db,
    name: "joinevents"
  }));
  if (!await app.service("joinevents").find().length) {
    app.service("joinevents").create({
      id: 1,
      eid: 1,
      uid: 170
    });
  }
  app.service("joinevents").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.requireAuth()
      ],
      create: [
        securityHooks.audit()
      ],
      patch: [
        securityHooks.requireRole("Admin")
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};
