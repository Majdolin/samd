const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
    if (!await db.schema.hasTable("groups")) {
        await db.schema.createTable("groups", table => {
            table.string("name");
            table.increments("id");
        });
    }
}

module.exports = async app => {
    await createTable();
    app.use("/groups", fknex({
        Model: db,
        name: "groups"
    }));
    app.service("groups").hooks({
        before: {
            all: [
                securityHooks.requireAuth()
            ],
            remove: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            create: [
                securityHooks.requireRole("Vorstand") // ("Admin")
            ],
            patch: [
                securityHooks.requireRole("Mitglied")
            ],
            update: [
                securityHooks.disable()
            ]
        }
    });
};

