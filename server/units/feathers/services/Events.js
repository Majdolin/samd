const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  if (!await db.schema.hasTable("events")) {
    await db.schema.createTable("events", table => {
      table.increments("id");
      table.text("title").notNullable().defaultTo("");
      table.text("desc").defaultTo("");
      table.text("date").defaultTo("");
      table.text("location").defaultTo("");
      table.text("address").defaultTo("");
      table.text("file1").defaultTo("");
      table.text("file2").defaultTo("");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/events", fknex({
    Model: db,
    name: "events"
  }));
  if (!await app.service("events").find().length) {
    app.service("events").create({
      id: 1,
      title: "# Event Test!",
      desc: "# Event Test!<br>Event",
      date: "",
      address: "",
      location: "",
      file1: "",
      file2: ""
    });
  }
  app.service("events").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.requireAuth()
      ],
      create: [
        securityHooks.requireAuth()
      ],
      patch: [
        securityHooks.requireRole("Admin")
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};
