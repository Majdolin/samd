const Promise = require("bluebird");
const local = require("@feathersjs/authentication-local");
const hookDistinct = require("../../../../utils/hookDistinct");
const parse = require("csv-parse/lib/sync");
const path = require("path");
const fs = require("fs");
const db = require("../../../../utils/knex");
const fknex = require("feathers-knex");
const hookJsonArray = require("./hookJsonArray");
const hookLowercase = require("./hookLowercase");
const hookActive = require("./hookActive");
const hookConfirm = require("./hookConfirm");
const hookMid = require("./hookMid");
const securityHooks = require("../../../../utils/securityHooks");

async function createTable() {
  const tableName = "users";
  // await db.schema.dropTableIfExists(tableName);
  if (!await db.schema.hasTable(tableName)) {
    await db.schema.createTable(tableName, table => {
      // db id
      table.increments("id").primary();
      // member id
      table.integer("mid").unique().notNullable();
      table.string("mail", 150).unique().notNullable();
      table.string("password", 128);
      table.string("address1", 200);
      table.string("address2", 200);
      table.string("firstName", 20).notNullable();
      table.string("lastName", 50).notNullable();
      table.string("zip", 5);
      table.string("city", 50);
      table.string("phone", 30);
      table.date("dob");
      table.string("studyField", 150);
      table.string("voice", 30);
      table.string("voiceGroup", 3);
      table.string("gender", 30);
      table.string("pledge", 30);
      table.date("lastLogin");
      // table.boolean("active");
      table.json("activeSemesters").defaultTo("[]");
      table.json("roles").defaultTo("[]");
      table.json("presets").defaultTo("[]");
    });
    return true;
  }
  return false;
}

function mapEntry(entry) {
  const activeSemesters = entry.Semester.split(/,\W*/);
  const roles = entry.Rolle ? entry.Rolle.split(/,\W*/) : [];
  roles.push("Mitglied");

  const result = {
    mid: parseInt(entry["MNr."], 10),
    address1: entry.Adresszeile1,
    address2: entry.Adressezeile2,
    firstName: entry.Vorname,
    lastName: entry.Nachname,
    gender: entry.Anrede,
    zip: entry.PLZ,
    city: entry.Ort,
    phone: entry.Telefon,
    mail: entry["E-Mail"],
    dob: /^\d{4}-\d\d-\d\d$/.test(entry.Geburtsdatum) ? entry.Geburtsdatum : undefined,
    studyField: entry.Studiengang,
    voice: entry.Stimme,
    voiceGroup: entry.Stimmgruppe,
    pledge: entry.Notenpfand,
    lastLogin: null,
    // active,
    activeSemesters,
    roles
  };

  Object.keys(result).forEach(key => {
    if (result[key] === "") {
      delete result[key];
    }
  });

  return result;
}

function importData() {
  const csvpath = path.resolve(__dirname, "..", "..", "..", "..", "..", "import", "aktive-mitglieder.csv");
  const content = fs.readFileSync(csvpath).toString("utf-8");
  const parsed = parse(content, { columns: true, delimiter: ";" });
  const result = parsed.map(mapEntry);
  return result;
}

module.exports = async app => {
  const newTable = await createTable();
  // app.use("users", memory());
  app.use("users", fknex({
    Model: db,
    name: "users"
  }));
  // apply hooks
  app.service("users")
    .hooks({
      before: {
        all: [
          // require user to be logged in
          securityHooks.requireAuth(),
          // handle JSON conversion
          hookJsonArray("roles"),
          hookJsonArray("presets"),
          hookJsonArray("activeSemesters"),
          // ensure mail is lowercase
          hookLowercase("mail"),
          // for password changes: hash password
          local.hooks.hashPassword({
            passwordField: "password"
          })
        ],
        update: [
          // disallow update
          securityHooks.disable()
        ],
        remove: [
          // only Vorstand may create users
          securityHooks.requireRole("Admin"),
        ],
        create: [
          // only Vorstand may create users
          securityHooks.requireRole("Vorstand"),
          // ensure member id is present on create
          hookMid()
        ],
        patch: [
          // manage what can be done by whom
          securityHooks.keepPropsByRole({
            Vorstand: [
              "mail", "address1", "address2", "firstName", "lastName", "zip",
              "city", "phone", "dob", "studyField", "voice",
              "voiceGroup", "gender", "pledge",
            ],
            Admin: ["roles"]
          }, [
            "presets", "password", "address1", "address2", "zip", "city", "phone", "mail", "active", "gender", "pledge"
          ]),
          // handle switching semester confirmation
          hookConfirm()
        ],
        find: [
          // do not allow getting other documents of not Vorstand
          securityHooks.restrictToOwnIfNotRole("Vorstand"),
          // allow getting distinct values
          hookDistinct(),
          // allow getting active semester state
          hookActive()
        ],
        get: [
          // do not allow getting other documents of not Vorstand
          securityHooks.restrictToOwnIfNotRole("Vorstand"),
          // allow getting active semester state
          hookActive()
        ]
      },
      after: {
        all: [
          // handle JSON conversion
          hookJsonArray("roles"),
          hookJsonArray("presets"),
          hookJsonArray("activeSemesters"),
          // never reveal password
          local.hooks.protect("password"),
        ]
      }
    });
  if (newTable) {
    const service = app.service("users");
    await Promise.map(importData(),
      data => service.create(data), {
        concurrency: 1
      });
  }
};
