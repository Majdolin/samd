/* eslint-disable no-param-reassign */
module.exports = field => hook => {
  if (
    hook.type === "before" &&
    hook.data !== undefined &&
    typeof hook.data[field] === "string"
  ) {
    hook.data[field] = hook.data[field].toLowerCase();
  }
  return hook;
};
