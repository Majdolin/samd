/* eslint-disable no-param-reassign */
module.exports = () => async hook => {
  if (
    hook.type === "before" &&
    hook.data !== undefined &&
    hook.method === "create" &&
    typeof hook.data.mid !== "number"
  ) {
    // get largest mid
    const [{ mid }] = await hook.service.find({
      query: {
        $select: ["mid"],
        $sort: {
          mid: -1
        },
        $limit: 1
      }
    });
    hook.data.mid = mid + 1;
  }
  return hook;
};
