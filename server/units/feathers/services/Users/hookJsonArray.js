/* eslint-disable no-param-reassign */
module.exports = field => hook => {
  if (hook.type === "before") {
    // "before" hook
    if (hook.data !== undefined && hook.data[field] !== undefined) {
      hook.data[field] = hook.data[field] ? JSON.stringify(hook.data[field]) : [];
    }
  } else if (hook.type === "after") {
    // "after" hook
    if (hook.result === undefined) {
      return hook;
    }
    if (Array.isArray(hook.result)) {
      hook.result.forEach(entry => {
        if (entry[field] !== undefined) {
          entry[field] = JSON.parse(entry[field]);
        }
      });
    } else if (hook.result[field] !== undefined) {
      hook.result[field] = JSON.parse(hook.result[field]);
    }
  }
  return hook;
};
