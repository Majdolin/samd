/* eslint-disable no-param-reassign */
// const knex = require("knex");
const knex = require("../../../../utils/knex");

module.exports = () => async hook => {
  if (
    hook.type !== "before" ||
    hook.params === undefined ||
    !hook.params.query ||
    !hook.params.query.$addActive
  ) {
    return hook;
  }
  delete hook.params.query.$addActive;

  const [{ type, year }] = await hook.app.service("semester").find();

  const kquery = hook.service.createQuery(hook.params);
  // remove distinct value from query
  kquery.select(knex.raw("instr(activeSemesters, ?) > 0 as active", [`${type}${year.toString().substr(2)}`]));
  if (hook.id !== undefined) {
    kquery.where("id", hook.id);
  }
  hook.params.knex = kquery;
  return hook;
};
