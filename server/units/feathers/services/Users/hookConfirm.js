/* eslint-disable no-param-reassign */
module.exports = () => async hook => {
  if (hook.type === "before") {
    // "before" hook
    if (hook.data !== undefined && hook.data.active !== undefined) {
      const [{ type, year }] = await hook.app.service("semester").find();
      const semester = type + year.toString().substr(2);
      const { activeSemesters } = await hook.service.get(hook.id, {
        query: {
          $select: ["activeSemesters"]
        }
      });
      let semesters = new Set(activeSemesters);
      if (hook.data.active) {
        semesters.add(semester);
      } else {
        semesters.delete(semester);
      }
      semesters = Array.from(semesters);
      const mapSort = val => val.substr(2) + (val[0] === "W" ? "X0" : "X1");
      semesters.sort((a, b) => {
        const vA = mapSort(a);
        const vB = mapSort(b);
        if (vA < vB) {
          return -1;
        }
        if (vB < vA) {
          return 1;
        }
        return 0;
      });
      await hook.service.patch(hook.id, {
        activeSemesters: semesters
      });
      hook.result = {
        success: true
      };
    }
  }
  return hook;
};
