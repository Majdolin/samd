const db = require("../../../utils/knex");
const fknex = require("feathers-knex");
const securityHooks = require("../../../utils/securityHooks");


async function createTable() {
  if (!await db.schema.hasTable("news")) {
    await db.schema.createTable("news", table => {
      table.increments("id");
      table.text("text").notNullable().defaultTo("");
    });
  }
}

module.exports = async app => {
  await createTable();
  app.use("/news", fknex({
    Model: db,
    name: "news"
  }));
  if (!await app.service("news").find().length) {
    app.service("news").create({
      id: 1,
      text: "# Herzlich Willkommen!",
    });
  }
  app.service("news").hooks({
    before: {
      all: [
        securityHooks.requireAuth()
      ],
      remove: [
        securityHooks.disable()
      ],
      create: [
        securityHooks.disable()
      ],
      patch: [
        securityHooks.requireRole("Admin")
      ],
      update: [
        securityHooks.disable()
      ]
    }
  });
};
