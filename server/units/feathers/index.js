/* eslint-disable no-param-reassign, consistent-return, global-require */
const feathers = require("@feathersjs/feathers");
const express = require("@feathersjs/express");

const nconf = require("nconf");
// const LocalStorage = require("node-localstorage").LocalStorage;
const path = require("path");
const fs = require("fs");
// const commonHooks = require("feathers-hooks-common");
const auth = require("@feathersjs/authentication");
const local = require("@feathersjs/authentication-local");
const jwt = require("@feathersjs/authentication-jwt");

const moment = require("moment");

function init() {
  const app = express(feathers());

  // Parse HTTP JSON bodies
  app.use(express.json());
  // Parse URL-encoded params
  app.use(express.urlencoded({ extended: true }));
  // REST provider
  app.configure(express.rest());
  // error handliner
  app.use(express.errorHandler());
  // add authentication
  app.configure(auth({
    session: false,
    local: {
      usernameField: "mail",
      passwordField: "password"
    },
    cookie: {
      enabled: false,
      secure: false
    },
    secret: nconf.get("server:secret")
  }));
  app.service("authentication")
    .hooks({
      before: {
        create: [
          auth.hooks.authenticate(["jwt", "local"])
        ],
        remove: [
          auth.hooks.authenticate("jwt")
        ]
      },
      after: {
        create: [
          async (hook) => {
            // update last login
            if (hook.params && hook.params.user && hook.params.authenticated) {
              await hook.app.service("users").patch(hook.params.user.id, {
                lastLogin: moment().format("YYYY-MM-DD")
              });
            }
            return hook;
          }
        ]
      }
    });
  // load all services in the "services" directory
  return Promise.all(fs.readdirSync(path.resolve(__dirname, "services"))
  // eslint-disable-next-line import/no-dynamic-require
    .map(service => Promise.resolve(require(path.join(__dirname, "services", service))(app))))
    .then(() => {
      app.configure(jwt());
      app.configure(local());

      if (nconf.get("server:wpbuild")) {
        const wpRouter = new express.Router();
        // eslint-disable-next-line global-require
        require("./webpack")(wpRouter);
        app.use("/", wpRouter);
      } else {
        app.use("/",
          express.static(path.resolve(__dirname, "..", "..", "..", "build")));
      }
      const port = nconf.get("server:port");
      app.listen(port);
      log.info("listening at http://localhost:%d", port);
    })
    .then(() => {
      // setup feathers error handler
      app.use(express.errorHandler());
      // generic error handler
      app.use((req, res) => {
        res.status(404).end("not found");
      });
    });
}

module.exports.run = init;

