/* eslint-disable global-require */

module.exports = router => {
  const webpack = require("webpack");
  const webpackConfig = require("../../../webpack.config");

  const hmr = true;

  if (hmr) {
    webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
    webpackConfig.plugins.push(new webpack.NoEmitOnErrorsPlugin());
    webpackConfig.entry.app.push("webpack-hot-middleware/client");
  }
  const compiler = webpack(webpackConfig);

  router.use(require("webpack-dev-middleware")(compiler, {
    noInfo: !hmr,
    publicPath: webpackConfig.output.publicPath
  }));

  if (hmr) {
    // eslint-disable-next-line global-require
    router.use(require("webpack-hot-middleware")(compiler));
  }
};
