/* eslint-disable no-param-reassign */
// creates a hook that allows filtering for distinct values
module.exports = () => (hook) => {
  const { params: { query } } = hook;

  // Must be a before hook
  if (hook.type !== "before") {
    throw new Error("The \"distinct\" hook should only be used as a \"before\" hook.");
  }

  // must be used on "find" method
  if (hook.method !== "find") {
    throw new Error("The \"distinct\" hook should be only used for get requests");
  }

  if (!query) {
    return hook;
  }

  // Throw error when no field is provided - eg. just users?$distinct
  if (query.$distinct === "") {
    throw new Error("Missing $distinct: Which field should be distinct?");
  }

  // get distinct value
  const distinctValue = query.$distinct;
  if (!distinctValue) {
    // no distinct value requested, return regular results
    return hook;
  }
  delete query.$distinct;

  const kquery = hook.service.createQuery(hook.params);
  // remove distinct value from query
  kquery.clearSelect().distinct(distinctValue).whereNotNull(distinctValue);

  hook.params.knex = kquery;
  return hook;
};
