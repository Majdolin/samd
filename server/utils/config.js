const path = require("path");
const nconf = require("nconf");

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = "development";
}

const rootPath = path.join(__dirname, "..", "..");

const yaml = require("js-yaml");
// define yaml format for parsing config.yml and custom.yml
const yamlFormat = {
  stringify: obj => yaml.safeDump(obj),
  parse: str => yaml.safeLoad(str)
};

// setup config
nconf
// use environment variable support
  .env()
// use command line argument override
  .argv()
// use config file
  .file("custom", { file: path.join(rootPath, "custom.yml"), format: yamlFormat })
  .file("config", { file: path.join(rootPath, "config.yml"), format: yamlFormat })
// setup defaults
  .defaults({
    mode: "default",
    debug: (process.env.NODE_ENV !== "production")
  });


module.exports = nconf;
