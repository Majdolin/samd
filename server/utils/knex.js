const knex = require("knex");
const path = require("path");

module.exports = knex({
  client: "sqlite3",
  connection: {
    filename: path.resolve(__dirname, "..", "..", "db.sqlite")
  },
  // required for sqlite (http://knexjs.org/#Builder-insert)
  useNullAsDefault: true
});
