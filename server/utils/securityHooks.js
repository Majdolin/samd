/* eslint-disable arrow-body-style, prefer-destructuring, no-param-reassign */
// security-related hooks

const common = require("feathers-hooks-common");
// const auth = require("feathers-authentication-hooks");
const authenticate = require("@feathersjs/authentication").hooks.authenticate;
const { Forbidden } = require("@feathersjs/errors");

/**
 * disable all external access to this method
 */
module.exports.disable = () => hook => {
  return common.disallow("external")(hook);
};

/**
 * require the user to be authenticated
 */
module.exports.requireAuth = () => authenticate("jwt");

/**
 * require specified role
 * @param {string} role role that is required. Admin is always allowed.
 */
module.exports.requireRole = role => hook => {
  if (!hook.params.provider) {
    // skip internal
    return hook;
  }
  const roles = hook.params.authenticated ? hook.params.user.roles : [];
  if (roles.indexOf("Admin") >= 0) {
    return hook;
  }
  if (roles.indexOf(role) === -1) {
    return Promise.reject(new Forbidden("user does not have required roles"));
  }
  return hook;
};

/**
 * require specified role, otherwise find() and get() only return
 * own items
 * @param {string} role role that allows to retrieve any document
 */
module.exports.restrictToOwnIfNotRole = role => hook => {
  if (!hook.params.provider) {
    // skip internal
    return hook;
  }
  const roles = hook.params.authenticated ? hook.params.user.roles : [];
  if (roles.indexOf("Admin") >= 0 || role.indexOf(roles) >= 0) {
    return hook;
  }
  const user = hook.params.authenticated ? hook.params.user : {
    id: false,
    roles: []
  };
  if (hook.method === "get" && parseInt(hook.id, 10) !== user.id) {
    return Promise.reject(new Forbidden("not allowed to get this"));
  }
  // limit search filter
  const kquery = hook.service.createQuery(hook.params);
  kquery.where("id", user.id);
  hook.params.knex = kquery;
  return hook;
};

/**
 * patch hook
 * whitelist properties to manage by roles
 * @param props role -> prop mapping
 * @param userprops list of props a user may change on himself
 * @return {function(*): *}
 */
module.exports.keepPropsByRole = (props, userprops) => hook => {
  if (!hook.data || !hook.params.provider) {
    return hook; // nothing to verify
  }
  const user = hook.params.authenticated ? hook.params.user : {
    id: false,
    roles: []
  };
  const finalProps = new Set();
  // add role properties
  user.roles.forEach(r => {
    if (Array.isArray(props[r])) {
      props[r].forEach(propname => finalProps.add(propname));
    }
  });
  // add user-defined properties if working on own dataset
  if (hook.id && parseInt(hook.id, 10) === user.id && Array.isArray(userprops)) {
    // allow user-defined props
    userprops.forEach(p => finalProps.add(p));
  }
  // validate properties
  Object.keys(hook.data).forEach(k => {
    if (!finalProps.has(k)) {
      // disallowed property
      delete hook.data[k];
    }
  });
  return hook;
};

/**
 * hook that logs user access to functions
 * @return {Function}
 */
module.exports.audit = () => async hook => {
  if (
    !hook.params.provider ||
    !hook.params.authenticated ||
    hook.method === "get" ||
    hook.method === "find" ||
    ["authentication"].indexOf(hook.path) >= 0 ||
    !hook.data ||
    !hook.params.user.mid
  ) {
    return hook;
  }
  const payload = JSON.parse(JSON.stringify(hook.data || {}));
  if (payload.password) {
    payload.password = "<obfuscated>";
  }
  const doc = {
    timestamp: Math.floor(Date.now() / 1000),
    mid: hook.params.user.mid,
    payload: JSON.stringify(payload),
    method: hook.method,
    service: hook.path
  };
  if (hook.id) {
    // specific document
    doc.target = hook.id;
  }
  await hook.app.service("audit").create(doc);
  return hook;
};

