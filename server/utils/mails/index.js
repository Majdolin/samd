/* eslint-disable func-names, one-var, prefer-destructuring, no-param-reassign */

const path = require("path");
const Promise = require("bluebird");
const EmailTemplate = require("email-templates");
const log = require("../log");
const nconf = require("nconf");
const nodemailer = require("nodemailer");
const smtpTransport = require("nodemailer-smtp-transport");

const templatedir = path.resolve(__dirname, "templates");
let transporter;
/**
 * send templated mail
 * @param  {string} recipient recipient of mail
 * @param  {string} template  template name
 * @param  {Object} options   template options
 * @return {Promise}           promise resolve when mail was sent successfully
 */
module.exports.sendTemplate = function (recipient, template, options) {
  return module.exports.template(template, options)
    .then(result => module.exports.send(recipient, result.subject, result.text, result.html));
};
/**
 * send email
 * @param  {string} recipient mail address of recipient
 * @param  {string} subject   subject of mail
 * @param  {string} text      optional plain text part of mail
 * @param  {string} html      optional HTML part of mail
 * @return {Promise}           promise which resolves if mail was sent successfully
 */
module.exports.send = function (recipient, subject, text, html) {
  // ensure transport
  if (!transporter) {
    transporter = nodemailer.createTransport(smtpTransport({
      ...nconf.get("email:config")
    }));
    // promisify transporter function
    transporter.sendMail = Promise.promisify(transporter.sendMail, { context: transporter });
  }
  const options = {
    from: nconf.get("email:from"),
    to: recipient,
    subject
  };
  if (text) {
    options.text = text;
  }
  if (html) {
    options.html = html;
  }
  return transporter.sendMail(options);
};
if (nconf.get("email:debug")) {
  // setup dummy mailer
  module.exports.send = function (recipient, subject, text, html) {
    log.info("new mail '%s' to %s: \n%s", subject, recipient, text || html);
    return Promise.resolve(true);
  };
}
/**
 * generate mail from template
 * @param  {string} template template name
 * @param  {Object} options  options provided to template
 * @return {Promise}          resolves to object with text, html and subject keys
 */
module.exports.template = function (template, options) {
  return new EmailTemplate({
    views: {
      root: templatedir
    }
  }).render(template, options)
    .then(results => {
    // retrieve subject
      let index,
        subject = "missing";
      if (!/<title>/.test(results)) {
      // get index of first linebreak
        index = results.indexOf("\n");
        // first line is subject
        subject = results.substr(0, index);
        // remove title from mail
        results = results.substr(index + 1);
        return {
          subject,
          text: results
        };
      } else { // eslint-disable-line
      // no text found, get subject from HTML title
        index = /<title>([\w\W]+?)<\/title>/mi.exec(results);
        if (index) {
          subject = index[1];
        }
        return {
          subject,
          html: results
        };
      }
    });
};
